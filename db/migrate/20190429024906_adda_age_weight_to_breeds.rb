class AddaAgeWeightToBreeds < ActiveRecord::Migration[5.2]
  def change
    add_column :breeds, :weight_avg, :float, default: 0
  end
end
