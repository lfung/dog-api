class AddCounterCacheToDogsBreed < ActiveRecord::Migration[5.2]
  def change
    add_column :breeds, :dogs_count, :integer, default: 0
  end
end
