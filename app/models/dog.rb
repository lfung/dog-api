class Dog < ApplicationRecord
  belongs_to :breed, counter_cache: true

  around_save :calculate_breed_age_average, :calculate_breed_weight_average

  validates :name, :age, presence: true
  validates :age, :weight, :numericality => { :greater_than_or_equal_to => 0 }

  scope :by_name, -> ( name ) { where('lower(name) = ?', name.downcase ) }
  scope :by_age, -> ( age ) { where('age = ?', age ) }
  scope :by_breed, -> ( breed_name ) { joins(:breed).where('lower(breeds.name) = ?', breed_name.downcase) }
  scope :by_weight, -> ( weight ) { where( 'weight = ?', weight) }
  scope :reserved, -> ( reserved ) { where( reserved: reserved )}

  # General Sort
  # Usage: Dog.order_by "name asc, age desc"
  scope :order_by, -> ( params ) { order( params ) }

  private

  def calculate_breed_age_average
    if breed_id_changed?
      object_after = Breed.find breed_id
      new_avg = object_after.dogs.average(:age)
      object_after.update_attributes( age_avg: new_avg )

      if breed_id_was
        object_before = Breed.find breed_id_was
        old_avg = object_before.dogs.average(:age)
        object_before.update_attributes( age_avg: old_avg )
      end
    end
    yield
  end

  def calculate_breed_weight_average
    if breed_id_changed?
      object_after = Breed.find breed_id
      new_avg = object_after.dogs.average(:weight)
      object_after.update_attributes( weight_avg: new_avg )

      if breed_id_was
        object_before = Breed.find breed_id_was
        old_avg = object_before.dogs.average(:weight)
        object_before.update_attributes( weight_avg: old_avg )
      end
    end
    yield
  end
end
