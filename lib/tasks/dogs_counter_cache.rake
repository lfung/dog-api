desc 'Update counter cache for Dogs'

task dogs_counter: :environment do
  Breed.reset_column_information
  Breed.select(:id).find_each do |p|
    Breed.reset_counters p.id, :dogs
  end
end
