# README

Dogs API test.

Setup:

- bundle install

- rake db:migrate

- rake db:seed

- rake rswag:specs:swaggerize

Start Rails Server:

- rails s

API documentation & Test:

- Go to http://localhost:3000/api-docs

Run automated test:

- rails test
