require 'swagger_helper'

describe 'Dogs Running S.A. API' do

  path '/api/v1/dogs' do

    post 'Create dog' do
      tags 'Dogs'
      consumes 'application/json', 'application/xml'
      parameter name: :dog, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          breed_id: { type: :integer },
          age: { type: :integer },
          weight: { type: :integer },
          reserved: { type: :boolean }
        },
        required: [ 'name', 'age' ]
      }
      response '201', 'dog created' do
        let(:dog) { { name: 'Fido', age: 2, weight: 6, breed_id: 1 } }
        run_test!
      end

      response '422', 'invalid request' do
        let(:dog) { { name: 'Lacy' } }
        run_test!
      end
    end
  end

  path '/api/v1/dogs/' do

    get 'List Dogs' do
      tags 'Dogs'
      produces 'application/json', 'application/xml'
      parameter name: :by_name, in: :query, type: :string, description: 'Name of the dog, ej. Kiwi'
      parameter name: :by_breed, in: :query, type: :string, description: 'Name of the breed, ej. Boxer'
      parameter name: :by_age, in: :query, type: :integer, description: 'Age of the dogs in years, ej. 2'
      parameter name: :by_weight, in: :query, type: :integer, description: 'Weight of the dogs in kg, ej. 7'
      parameter name: :reserved, in: :query, type: :boolean, description: 'Reserved dogs, ej. true/1 or false/0'
      parameter name: :sort, in: :query, type: :string, description: 'Sort Result: ej. sort by name desc: ?sort=+name, now asc: ?sort:-name'

      response '200', 'name found' do
        schema type: :object,
          properties: {
            id: { type: :integer, },
            name: { type: :string },
            age: { type: :integer },
            weight: { type: :integer },
            reserved: { type: :boolean }
          }

        # let(:id) { Pet.create(name: 'fofo', age: 2, weight: 4).id }
        run_test!
      end

      response '200', 'list of  dogs' do
        run_test!
      end

    end
  end

  path '/api/v1/dogs/{id}/reserve' do

    get 'Reserve a Dog' do
      tags 'Dogs'
      produces 'application/json', 'application/xml'
      parameter name: :id, in: :path, type: :string, required: true, description: 'Dogs id, ej: ?id=1'

      response '200', 'Dog already reserved' do
        run_test!
      end
    end
  end

  path '/api/v1/dogs/summary' do

    get 'Get Dogs Summary' do
      tags 'Dogs'
      produces 'application/json', 'application/xml'

      response '422', 'Dog already reserved' do
        run_test!
      end

      response '200', 'Dog reserved' do
        run_test!
      end
    end
  end
end
