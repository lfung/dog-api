class AddaAgeAvgToBreeds < ActiveRecord::Migration[5.2]
  def change
    add_column :breeds, :age_avg, :float, default: 0
  end
end
