class CreateDogs < ActiveRecord::Migration[5.2]
  def change
    create_table :dogs do |t|
      t.string :name, null: false
      t.references :breed, foreign_key: true
      t.integer :age
      t.integer :weight
      t.date :reservation_date_limit

      t.timestamps
    end
  end
end
