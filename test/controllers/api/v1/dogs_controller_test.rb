require 'test_helper'

class Api::V1::DogsControllerTest < ActionDispatch::IntegrationTest
  test "create a dog" do
    dog_params = {
      name: 'Lia',
      age: 3,
      weight: 5,
      breed_id: 1
    }

    post api_v1_dogs_path, params: { dog: dog_params }

    dog = JSON.parse(@response.body)['data']

    assert_response :success

    assert dog['name'] == dog_params[:name]
  end

  test "reserve a dog" do
    # get reserve_api_v1_dog_path, params: { id: '1' }

    get "/api/v1/dogs/1/reserve"

    dog = JSON.parse(@response.body)['data']

    assert_response :success
  end
end
