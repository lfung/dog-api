class AddReservationToDogs < ActiveRecord::Migration[5.2]
  def change
    add_column :dogs, :reserved, :boolean, default: false, null: false
  end
end
