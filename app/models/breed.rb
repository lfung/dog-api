class Breed < ApplicationRecord

  has_many :dogs

  validates :name, presence: true

  def age_avgr
    dogs.average(:age).to_f
  end

  def to_s
    name
  end

end
