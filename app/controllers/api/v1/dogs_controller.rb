class Api::V1::DogsController < ApplicationController
	include Orderable

	before_action :get_dog, only: [:show, :update, :destroy, :reserve]

	has_scope :by_name, only: :index
	has_scope :by_breed, only: :index
	has_scope :by_age, only: :index
	has_scope :by_weight, only: :index
	has_scope :reserved, only: :index

	def index
		json = Rails.cache.fetch(params.to_s) do
			apply_scopes(Dog).order(ordering_params(params))
		end
		paginate json: json
	end

	def create
		@dog = Dog.new(dog_params)
		if @dog.save
			render json: { message: 'Dog created', data: @dog }, status: :created
		else
			render json: { message: 'Dog not saved', data: @dog.errors }, status: :unprocessable_entity
		end
	end

	def destroy
		if @dog.destroy
			head(:ok)
		else
			head(:unprocessable_entity)
		end
	end

	def summary
		render json: { message: 'Data summary', dogs_by_breed: { message: 'Number of dogs by Breed',
			dogs: Dog.all.joins(:breed).select('dogs.name, breeds.name as breed, dogs.age, dogs.weight, dogs.reserved, breed_id'),
			beeds: Breed.all.select(:id, :name, :dogs_count, :age_avg, :weight_avg) } }
	end

	def reserve
		if @dog.reserved
			render json: { message: 'Dog already reserved', data: @dog }, status: :unprocessable_entity
		else
			days_param = params[:days].to_i
			days = days_param > 0 ? days_param : 2
			@dog.update_attributes(reservation_date_limit: Date.today + days.day, reserved: true)
			render json: { message: 'Dog reserved', dog: @dog }, status: :ok
		end
	end


	private

	def get_dog
		@dog = Dog.find(params[:id])
	end

	def dog_params
		params.require(:dog).permit(:name, :breed_id, :age, :weight, :reservation_date_limit)
  end

end
